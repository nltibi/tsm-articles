# Sisteme de mesagerie performante -- Apache Kafka

Odată cu răspândirea arhitecturilor bazate pe evenimente, sistemele de mesagerie au
devenit componentele de bază a arhitecturilor enterprise. Deoarece aplicațiile
enterprise prelucrează din ce în ce mai multe date, performanța sistemelor de
mesagerie devine din ce în ce mai importantă pentru buna funcționare a
aplicațiilor, necesitând platforme rapide și scalabile. Apache Kafka este un
sistem nou mesagerie, care se remarcă ca fiind una dintre cele mai performante
soluții la momentul actual, putând transfera până la un milion de mesaje pe
secundă pe un grup de trei mașini de capacitate medie.

Kafka a fost dezvoltat inițial la LinkedIn, într-o perioadă în care LinkedIn-ul
migra de la o bază de date monolitică la o arhitectură bazată pe servicii, în
care fiecare serviciu își avea propriul lui model de stocare a datelor. Una
dintre probleme care s-a ivit în timpul migrării a fost distribuirea în timp
real a jurnalelor de acces de pe serverele web către serviciul de analiză a
activității utilizatorilor. Inginerii de la LinkedIn aveau nevoie de o platformă
care să poată transfera cantități mari de date către mai multe servicii, într-un
timp cat mai scurt. Platformele existente s-au dovedit a fi ineficiente pentru
volumul de date de care dispuneau, asa că și-au dezvoltat propriul lor sistem de
mesagerie, sub numele Kafka. Ulterior, proiectul a fost lansat *open source*, și
donat la *Apache Software Foundation*. După lansare Kafka a fost adoptat de mai
multe companii care aveau nevoi similare de transfer de mesaje. 

## Viteză vs funcționalități

Principalul obiectiv în proiectarea Kafka a fost maximizarea vitezei de transfer
al mesajelor. Pentru a obține viteze cât mai mari, Kafka vine cu un model de
publicare regândit, și renunță la câteva dintre facilitățile oferite de
platformele clasice de mesagerie. 

Una dintre cele mai importante schimbări o reprezintă la modul de retenție a
mesajelor publicate. Producătorii publică mesaje în Kafka,  care devin
disponibile pentru procesat consumatorilor, însă consumatorii nu trebuie să
confirme procesarea mesajelor. În schimb, Kafka reține toate mesajele primite
pentru o perioadă fixă de timp, consumatorii având libertatea de a consuma orice
mesaj reținut. Deși pare ineficient la prima vedere, acest model de lucru aduce
o serie de avantaje:

 - Simplifică mult arhitectura sistemului---Kafka nu trebuie să rețină care
   dintre mesaje au fost consumate și care nu
 - Izolează producătorii de consumatori. În sistemele în care consumatorii sunt
   obligați să confirme procesare mesajelor, performanța sistemului se poate
   degrada odată ce numărul de mesaje neconfirmate crește. Unele servicii
   limitează rata la care producătorii pot publica mesaje pentru a nu
   suprasolicita sistemul de mesagerie, însă această abordare poate duce la
   situații periculoase în care un consumator lent, dar neimportant afectează un
   producător critic pentru business.
 - Consumatorii nu trebuie sa consume permanent, ei putând fi opriți oricând,
   fără a impact sistemul de mesagerie. Ei pot fi chiar și *batch job*-uri
   executate periodic. Desigur, abordarea asta funcționează numai dacă mesajele
   sunt reținute în Kafka suficient de mult încât procesele respective sa apuce
   să prelucreze datele acumulate intre rulări.

Deoarece mesajele nu trebuiesc reținute selectiv, Kafka poate folosi un model de
stocare foarte simplu și eficient: *jurnalul de mesaje*. Jurnalul nu e decât o listă de
mesaje, în care mesaje noi sunt adăugate tot timpul la sfârșitul listei.
Mesajele existente nu se modifică niciodată.  Întrucât jurnalul se modifică
numai la sfârșit prin adăugarea de mesaje noi, ea se poate stoca optim pe medii
de stocare magnetice -- scrierea pe hard disk se poate face secvențial, evitând
astfel deplasarea capului de scriere, o operație costisitoare din punct de
vedere al performanței. De asemenea, dacă consumatorii reușesc sa țină pasul cu
producătorii de mesaje, Kafka poate servii mesajele direct din memorie,
folosindu-se în acest caz de mecanismele de caching oferite de sistemul de
operare.

O altă diferență importantă față de sistemele tradiționale o reprezintă modul în
care pot fi consumate mesajele. Sistemele tradiționale oferă două moduri de
consum: *coadă (queue)* și *publicare-abonare (publish-subscribe)*. În modelul
*coada*, fiecare mesaj ajunge la un singur consumator. În modelul
*publicare-abonare*, fiecare mesaj ajunge la fiecare consumator. Kafka vine cu o
singură abstractizare care acoperă ambele modele: *grupuri de consumatori*. În
Kafka, fiecare consumator face parte dintr-un grup, chiar dacă e singurul
consumator din grup. În cadrul grupului, numai un consumator poate primi un
mesaj. În schimb mesajele sunt livrate tuturor grupurilor de consumatori.
Această simplificare permite sistemului să ofere singură modalitate de a grupa
mesajele: *topica*. Modelul Kafka e de fapt un fel de *publish-subscribe*,
în care grupuri de consumatori și nu consumatori individuali se abonează la o
topică. Dacă toți consumatorii fac parte din același grup, fiecare masaj va
ajunge la  un singur consumator din grup, topica funcționând ca și o coadă de
mesaje. Dacă în schimb fiecare consumator face parte din alt grup, fiecare
consumator va primi mesajele publicate---modelul *publish-subscribe* clasic. În
practică însă, vom avea de a face cu un număr mic de grupuri de consumatori,
fiecare grup corespunzând de obicei unui serviciu interesat de datele publicate
în Kafka. În cadrul gropurilor vom avea mai mulți consumatori (de obicei câte
unul pentru fiecare mașină pe care rulează serviciul). Deoarece fiecare grup
primește fiecare mesaj publicat pe o topică, serviciile vor primi toate
mesajele publicate, dar în cadrul unui serviciu procesarea de mesaje se va
distribui intre mașini.

![Consumul de mesaje pentru 2 grupuri](groups_ro.svg)

## Arhitectura Kafka

În linii mari, un serviciu Kafka e format din mai multe mașini care au rolul de
broker, ele reținând mesajele publicate. Pe lângă agenți, mai e nevoie și de un
grup de 3-5 mașini care să ruleze Apache Zookeeper. Kafka folosește Zookeeper
pentru coordonarea și managementul brokerilor.

Deoarece volumul de mesajele publicate pe o topică poate depăși capacitatea unui
broker, topicele se împart la rândul lor în mai multe *partiții*. Fiecare
partiție e de fapt un jurnal de mesaje. Partițiile trebuie să încapă în
totalitate pe un broker, în schimb, partițiile ce țin de un topic sunt
distribuite uniform în cadrul brokerilor, fiecare broker găzduind un număr
aproximativ egal de partiții. 

![Alocarea de partiții pentru 2 brokeri](partitions_ro.svg)

Când un producător vrea să publice un mesaj în Kafka, producătorul interoghează un broker
pentru a afla câte partiții sunt și cum sunt ele distribuite în cadrul
brokerilor, decide în care dintre partiții să publice (pe baza conținutului
mesajului, aleatoriu, sau luând pârtiile pe rând), după care trimite mesajul
brokerului care găzduiește partiția aleasă. Pe partea de consumator însă,
lucrurile nu sunt așa de simple. Partițiile topicii sunt distribuite în mod egal
între consumatorii dintr-un grup, fiecărui consumator revenind una sau mai multe
partiții din care poate citii mesajele. Pe lângă distribuirea uniformă a
sarcinii de consum, alocarea partițiilor garantează că fiecare mesaj va fi
procesat de un singur consumatori din grup. Indexul ultimului mesaj procesat din
fiecare partiție se reține în Zookeeper, astfel că, dacă un consumator iese din
grup, partițiile lui pot fi realocate altor consumatori, care poate relua
procesarea de la ultimul mesaj citit.

Partiționarea distribuie uniform sarcina în rândul brokerilor, dar ce se
întâmplă în cazul în care un broker iese din grup sau devine inaccesibil?  În
funcție de natura problemei, partițiile găzduite de el pot deveni inaccesibile
sau se pot pierde definitiv. Pentru a preveni astfel de situații, Kafka
introduce conceptul de *partiții replica* (*replica partitions*). Fiecare
partiție găzduită de un broker (numită de acum încolo *partiție lider*) poate
avea una sau mai multe replici. Acestea sunt partiții la rândul lor, doar că
sunt stocate pe alți brokeri decât cea pe care se află partiția lider.
Producătorii nu pot publica în replici, numai în partiții lider, iar brokerii pe
care găzduiesc partițiile lider se asigură că orice mesaj primit e salvat și în
replici. În eventualitatea pierderii unui broker, unul dintre replicile fiecărei
partiții leader de pe brokerul pierdut se promovează la statutul de lider,
astfel încât Kafka continuă să funcționeze fără întreruperi și fără pierderi de
date. Când brokerul e repus în funcțiune, el își sincronizează partițiile de le
ceilalți brokeri, după care începe un proces de desemnare a liderilor pentru
fiecare partiție.

## Concluzii

Kafka reprezintă o soluție bună pentru aplicațiile care necesită o viteză mare
de transfer și o latență mică la livrarea mesajelor. Arhitectura lui simplă și
modalitatea flexibilă de grupare a consumatorilor îl fac potrivit pentru o serie
de aplicații: colectare de jurnale și metrici de performanță, procesare de
secvențe de date și evenimente.

Ca orice tehnologie, Kafka are și ea limitările ei. Faptul că mesajele nu pot fi
reprocesate individual poate reprezenta o problemă pentru unele tipuri de
aplicații. O altă problema o poate reprezenta lipsa  de programe și unelte de
administrare. Spre deosebire de alte platforme gen ActiveMQ sau RabbitMQ, Kafka
are un ecosistem slab dezvoltat. Necesitatea rulării unui cluster de Zookeeper
pe lângă brokerii Kafka poate de asemenea reprezenta un impediment financiar sau
administrativ. Sperăm însă că o parte dintre aceste limitări vor dispărea odată cu
răspândirea și maturizarea tehnologiei.
